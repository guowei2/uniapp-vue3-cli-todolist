# uniapp-vue3-todolist

uniapp cli版todolist

## 开发体验

问题：

1. button按钮，type="primary" 会提示类型错误,需要额外配置@uni-helper/uni-app-types处理 <https://github.com/dcloudio/uni-app/issues/3719>
2. 官方的单元测试用例跑不起来 提示 ReferenceError: program is not defined <https://ask.dcloud.net.cn/question/118839>

## 开发注意

1. 全局安装czg ,之后提交都用czg命令，这样更规范
